package drBunsen;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Random;

import net.minecraft.util.org.apache.commons.io.FileUtils;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * 
 * @author Dr. Bunsen
 * 
 * Contact me if you want to use any code or have questions regarding the code.
 *
 */
public class Information extends JavaPlugin {
	private HashMap<String, String> information;
	private String folderPath;
	
	public void onEnable() {
		folderPath = getDataFolder().getAbsolutePath() + "/";
		doIHaveAFolder();
		checkForConfig();
		processCommands();
		log("Information is fully loaded and ready to inform.");
	}
	 
	public void onDisable() { 
		log("Information is fully closed and done informing.");
	}
	
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
		if(cmd.getName().equalsIgnoreCase("sinfo")) {
			if (args.length == 0) {
				sendInformation(sender, "sinfo");
				return true;
			} else {
				args[0] = args[0].toLowerCase();
				sendInformation(sender, args[0]);
				return true;
			}
		} else if (cmd.getName().equalsIgnoreCase("sreload")) {
			processCommands();
			sender.sendMessage(ChatColor.GOLD + "The config file has been reloaded!");
			return true;
		} else if (cmd.getName().equalsIgnoreCase("sendinfo")) {
			sendInfo(sender, args);
			return true;
		}
		return false; 
	}
	
	/**
	 * Processes the commands found in the commands.yml
	 * Processing goes per category.
	 * While processing we will fix chat colours etc.
	 * 
	 */
	void processCommands() {
		information = new HashMap<String, String>();
		String[] lines = readFille("commands.yml").split("\n");
		String[] command;
		String[] desc;
		String[] info;
		String noArg = "";
		
		if (lines.length > 0) {
			command = new String[(lines.length +1) / 3];
			info = new String[(lines.length +1) / 3];
			desc = new String[(lines.length +1) / 3];
			int x = 0;
			
			//lets process the file
			for (String i : lines) {
				if (i.contains("{") && i.contains("}")) {
					i = i.replace("{", "");
					i = i.replace("}", "");
					i = i.toLowerCase();
					command[x] = i;
				} else if (i.contains("[") && i.contains("]")) {
					i = i.replace("[", "");
					i = i.replace("]", "");
					i = fixColours(i);
					desc[x] = i;
				} else {
					i = fixColours(i);
					info[x] = fixNewLine(i);
					++x;
				}
			}
			
			//and now fill the hashmap
			for (int i = 0; i < ((lines.length +1) / 3); ++i) {
				if (command[i].equals("sinfo")) {
					noArg = info[i];
					continue;
				}
				information.put(command[i], info[i]);
			}
			//And now for sinfo
			for(String i : desc) {
				if (i.equals("!IGNORE")) {
					continue;
				}
				noArg = noArg + "\n" + i;
			}
			information.put("sinfo", noArg);
		} else {
			information.put("info", "The commands.yml is empty!");
		}
	}	

	
	/**
	 * This outputs Strings in the console.
	 * 
	 * @param log
	 * String to be send to the console.
	 * 
	 */
	void log(String log) {
		System.out.println("[Information] " + log);
	}
	
	/**
	 * 
	 * A simple file loader.
	 * 
	 * @param fileName
	 * The name of the file to be loaded from the plugin's folder.
	 * 
	 * @return
	 * The file if found, null if it wasn't found.
	 * 
	 */
	private File loadFile(String fileName) {
		return new File(folderPath + fileName);
	}
	
	/**
	 * 
	 * Sends the given info to the given player.
	 * 
	 * @param s
	 * The one who asked to send info to a certain player
	 * @param args
	 * The arguments of the command, containing [0] the receiver of the command and [2] the requested information.
	 * 
	 */
	void sendInfo(CommandSender s, String args[]) {
		args[1] = args[1].toLowerCase();
		Player p = Bukkit.getPlayer(args[0]);
		if (validArg(args[1])) {
			if (p != null) {
				p.sendMessage(randomColour(information.get(args[1])));
			} else {
				s.sendMessage("The player: '" +args[0] + "' cannot be found.");
			}
		} else {
			s.sendMessage("The info: '" +args[1] + "' cannot be found.");
		}

	}
	
	
	/**
	 * 
	 * Creates the file that is given to it
	 * 
	 * @param f
	 * File to create
	 * 
	 * @return
	 * True if file was created, false if it wasn't
	 * 
	 * Unused at the moment, will implement later.
	 * 
	 */
	@SuppressWarnings("unused")
	private boolean createFile(File f) {
		try {
			return f.createNewFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	/**
	 * 
	 * Check whether the plugin folder exists.
	 * If it doesn't, it creates it.
	 * 
	 * @return
	 * True if a folder existed, or does exist now. False if it couldn't create a folder.
	 * 
	 */
	private boolean doIHaveAFolder() {
		File f = loadFile("");
		if (f.exists()) {
			return true;
		} else {
			return f.mkdirs();
		}
	}
	
	/**
	 * 
	 * Reads the given file from the plugin folder.
	 * 
	 * @param fileName
	 * The name of the wanted file.
	 * 
	 * @return
	 * The contents of the file in String format.
	 * 
	 */
	private String readFille(String fileName) {
		String output = "";
		File file = loadFile(fileName);
		
		if (file.exists()) {
			try {
				BufferedReader br = new BufferedReader(new FileReader(file));
				String line;
				while ((line = br.readLine()) != null) {
				   if (line.startsWith("#") || line.length() == 0) {
					   continue;
				   }
				   output = output + line + "\n";
				}
				br.close();
			} catch (Exception s) {
				s.printStackTrace();
			}
		}
		return output;
	}
	
	/**
	 * 
	 * This checks whether there is a commands.yml.
	 * If there isn't, it will copy the standard commands.yml from the jar to the plugin folder.
	 * 
	 */
	void checkForConfig() {
		File f = loadFile("commands.yml");
		if (!f.exists()) {
			URL inputUrl = getClass().getResource("/commands.yml");
			try {
				FileUtils.copyURLToFile(inputUrl, f);
				log("We made the commands.yml!");
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * 
	 * Replaces the !n with a \n, so the message actually gets break lines.
	 * 
	 * @param i
	 * String to fix.
	 * 
	 * @return
	 * The fixed String.
	 * 
	 */	
	private String fixNewLine(String i) {
		return i.replaceAll("!n", "\n");
	}
	
	/**
	 * Checks whether the given argument exists as an actual '/sinfo arg' attribute.
	 * 
	 * @param arg
	 * The wanted argument
	 * 
	 * @return
	 * True if the argument exists, false if it doesn't.
	 * 
	 */
	boolean validArg(String arg) {
		return information.get(arg) != null;
	}
	
	void sendInformation(CommandSender s, String asked) {
		if (validArg(asked)) {
			s.sendMessage(randomColour(information.get(asked)));
		} else {
			sendInformation(s, "sinfo");
		}
	}
	
	/**
	 * 
	 * Replaces a String his colour code to a colour code compatible format with bukkit.
	 * 
	 * @param toFix
	 * The String to fix.
	 * 
	 * @return
	 * The fixed String
	 * 
	 */
	private String fixColours(String toFix) {
		if (toFix.contains("<") && toFix.contains(">")) {
			//I cba about making this more efficient, this is one time only anyways.
			toFix = toFix.replaceAll("<black>", "" + ChatColor.BLACK);
			toFix = toFix.replaceAll("<dblue>", "" + ChatColor.DARK_BLUE);
			toFix = toFix.replaceAll("<daqua>", "" + ChatColor.DARK_AQUA);
			toFix = toFix.replaceAll("<dgreen>", "" + ChatColor.DARK_GREEN);
			toFix = toFix.replaceAll("<dred>", "" + ChatColor.DARK_RED);
			toFix = toFix.replaceAll("<dpurple>", "" + ChatColor.DARK_PURPLE);
			toFix = toFix.replaceAll("<gold>", "" + ChatColor.GOLD);
			toFix = toFix.replaceAll("<gray>", "" + ChatColor.GRAY);
			toFix = toFix.replaceAll("<dgray>", "" + ChatColor.DARK_GRAY);
			toFix = toFix.replaceAll("<blue>", "" + ChatColor.BLUE);
			toFix = toFix.replaceAll("<green>", "" + ChatColor.GREEN);
			toFix = toFix.replaceAll("<aqua>", "" + ChatColor.AQUA);
			toFix = toFix.replaceAll("<red>", "" + ChatColor.RED);
			toFix = toFix.replaceAll("<lpurple>", "" + ChatColor.LIGHT_PURPLE);
			toFix = toFix.replaceAll("<yellow>", "" + ChatColor.YELLOW);
			toFix = toFix.replaceAll("<white>", "" + ChatColor.WHITE);
			toFix = toFix.replaceAll("<magic>", "" + ChatColor.MAGIC);
			toFix = toFix.replaceAll("<bold>", "" + ChatColor.BOLD);
			toFix = toFix.replaceAll("<strike>", "" + ChatColor.STRIKETHROUGH);
			toFix = toFix.replaceAll("<line>", "" + ChatColor.UNDERLINE);
			toFix = toFix.replaceAll("<italic>", "" + ChatColor.ITALIC);
			toFix = toFix.replaceAll("<reset>", "" + ChatColor.RESET);
		}
		return toFix;
	}
	
	private String randomColour(String toFix) {
		
		if (toFix.contains("<random>")) {
			int random = generate(1,20);
			switch(random) {
			case 1:
				toFix = toFix.replaceAll("<random>", "" + ChatColor.BLACK);
				break;
			case 2:
				toFix = toFix.replaceAll("<random>", "" + ChatColor.DARK_BLUE);
				break;
			case 3:
				toFix = toFix.replaceAll("<random>", "" + ChatColor.DARK_AQUA);
				break;
			case 4:
				toFix = toFix.replaceAll("<random>", "" + ChatColor.DARK_GREEN);
				break;
			case 5:
				toFix = toFix.replaceAll("<random>", "" + ChatColor.DARK_RED);
				break;
			case 6:
				toFix = toFix.replaceAll("<random>", "" + ChatColor.DARK_PURPLE);
				break;
			case 7:
				toFix = toFix.replaceAll("<random>", "" + ChatColor.GOLD);
				break;
			case 8:
				toFix = toFix.replaceAll("<random>", "" + ChatColor.GRAY);
				break;
			case 9:
				toFix = toFix.replaceAll("<random>", "" + ChatColor.DARK_GRAY);
				break;
			case 10:
				toFix = toFix.replaceAll("<random>", "" + ChatColor.BLUE);
				break;
			case 11:
				toFix = toFix.replaceAll("<random>", "" + ChatColor.GREEN);
				break;
			case 12:
				toFix = toFix.replaceAll("<random>", "" + ChatColor.AQUA);
				break;
			case 13:
				toFix = toFix.replaceAll("<random>", "" + ChatColor.RED);
				break;
			case 14:
				toFix = toFix.replaceAll("<random>", "" + ChatColor.LIGHT_PURPLE);
				break;
			case 15:
				toFix = toFix.replaceAll("<random>", "" + ChatColor.YELLOW);
				break;
			case 16:
				toFix = toFix.replaceAll("<random>", "" + ChatColor.WHITE);
				break;
			case 17:
				toFix = toFix.replaceAll("<random>", "" + ChatColor.BOLD);
				break;
			case 18:
				toFix = toFix.replaceAll("<random>", "" + ChatColor.STRIKETHROUGH);
				break;
			case 19:
				toFix = toFix.replaceAll("<random>", "" + ChatColor.UNDERLINE);
				break;
			case 20:
				toFix = toFix.replaceAll("<random>", "" + ChatColor.ITALIC);
				break;
			}
		}
		return toFix;
	}
	
	/**
	 * 
	 * This generates a random number between the min and max BOTH min AND max ARE POSSIBLE OUTCOMES!!!
	 * @param min the minimal outcome. Int.
	 * @param max the maximal outcome. Int.
	 * @return A random number between min and max. Int.
	 * 
	 * @author
	 * Dr. Bunsen
	 * 
	 */
	public int generate(int min, int max) {
		Random r = new Random();
		int range = max - min + 1;
		return min + r.nextInt(range);
	}
	
}
